/*****************************************
 *
 *	FILE:		get_last_view_panel.4dm
 *
 *	DESCRIPTION:
 *  Displays the last active view.
 *  Mainly to be used for capturing the last view
 *  as a parameter for use in chains.
 *
 *
 *
 *	AUTHOR:		Matthew Monk
 *	STARTED:	30-10-2020
 *
 *	REVISION HISTORY:
 *	1		30-10-2020	Initial macro
 *
 */

#define DEBUG								0

// Prerequisites
// Standard 12d libraries
#include "set_ups.h"
#include "standard_library.h"

// #### GLOBAL VARIABLES ####
{
	// Appears in the panel title bar and about information printed to Output Window
	Text PROGRAM_NAME = "Get Last View";
	Text PROGRAM_AUTHOR = "Matthew Monk";
	Text PROGRAM_VERSION = "1.0";
	Text PROGRAM_DATE = "2020-10-30";
}

// ### Function Prototypes ###
Integer Manage_panel();

// ========== START ROUTINES ============


/*! @ingroup macro
 *	@brief
 *	Main entry point for macro.
 */
void main()
{
	Manage_panel();
	return;
}

/*! @ingroup macro
 *	@brief
 *	Displays a GUI panel and handles user interaction with panel.
 */
Integer Manage_panel()
{
	Integer rv = -1;

	Panel panel = Create_panel(PROGRAM_NAME, TRUE);
	// ### V & H GROUPS ###
	Vertical_Group vg_panel = Create_vertical_group(BALANCE_WIDGETS_OVER_HEIGHT);
	Horizontal_Group hg_buttons = Create_button_group();
	Colour_Message_Box message = Create_colour_message_box(" ");

	// ### MAIN ###
	Text last_view_name = "";
	View_Box view_box = Create_view_box("View", message, VIEW_EXISTS);
	Get_last_view(last_view_name);
	Set_data(view_box, last_view_name);
	Append(view_box, vg_panel);

	// ### BUTTONS ###
	Button but_process = Create_button("Last view", "process");
	Button but_finish = Create_finish_button("Finish", "finish");
	Button but_help = Create_help_button(panel, "Help");

	Append(but_process, hg_buttons);
	Append(but_finish, hg_buttons);
	Append(but_help, hg_buttons);

	Append(message, vg_panel);
	Append(hg_buttons, vg_panel);
	Append(vg_panel, panel);

	Show_widget(panel);

	Integer doit = 1;
	Text status_msg = "";

	while(doit)
	{
		Integer id = -1;
		Text    cmd = "";
		Text    msg = "";
		Integer ret = Wait_on_widgets(id,cmd,msg);  // this processes standard messages first ?

		rv = -1;

		if(cmd == "keystroke") continue;

		switch(id)
		{
			case Get_id(panel) :
			{
				if(cmd == "Panel Quit")
				{
					doit = 0;
				}
				if(cmd == "Panel About")
				{
					about_panel(panel);
				}
			} break;		// End - Panel

			case Get_id(but_finish) :
			{
				if(cmd == "finish") doit = 0;
			} break;		// End - finish

			case Get_id(but_process) :
			{
				last_view_name = "";
				rv = Get_last_view(last_view_name);
				if (rv == 0)
				{
					if (View_exists(last_view_name))
					{
						Set_data(view_box, last_view_name);
					}
					else
					{
						Set_data(view_box, "");
						status_msg = "Last view < " + last_view_name + " > does not exist.";
						Set_data(message, status_msg, MESSAGE_LEVEL_ERROR);
						Print("ERROR : " + status_msg + "\n");
					}
				}
				else
				{
					Set_data(view_box, "");
					status_msg = "Unable to get last view (err: " + To_text(rv) + ")";
					Set_data(message, status_msg, MESSAGE_LEVEL_ERROR);
					Print("ERROR : " + status_msg + "\n");
				}
			} break;		// End - process
		}
	}
	return 0;
}

