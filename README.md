# README

## get_last_view_panel

2020/10/30 - Matthew Monk

A custom application (aka macro) for 12d Model to display the last active view in a panel.

This app is primarily intended for use in chains for capturing, in parameters, the last active view. Refer to the sample chain and parameter file in the test directory.

- \bin - Compiled versions of the macro, both in a DEBUG and Release version. There is no real difference between the two for this simple macro.
- \lib - Library files.
- \src - Source files (.4dm) for the macro. This also includes various files for the Code::Blocks project- .cbp, .bmarks, .layout . These can be ignored.
- \test - Test chain and parameter file. Assumes the compiled macro (.4do) is in the same directory as the chain.


To compile:
1. Get a copy of standard_library.h from this download:  http://forums.12dmodel.com/downloads/Macros/includes.zip
2. Copy standard_library.h and the get_last_view_panel.4dm files in the same directory somewhere.
3. From 12d Model, run the Utilities->Macros->Compile option.
4. Select the get_last_view_panel.4dm file.
5. Click the Compile button.
6. Check the compile log and panel for errors.

Feel free to use and modify this code as desired. Refer to LICENSE for details.

Hope you find this useful!

MM